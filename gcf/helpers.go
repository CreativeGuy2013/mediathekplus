package gcf

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	b64 "encoding/base64"
)

//Video is a single video stored on the server
type Video struct {
	ID          string `json:"id"`
	Broadcaster string `json:"broadcaster"`
	Title       string `json:"title"`
	Series      string `json:"series"`
	Description string `json:"description"`
	Date        string `json:"date"`
	date        time.Time
	LibraryURL  string `json:"library_url"`
	Video       string `json:"video_sd"`
	VideoHD     string `json:"video_hd"`
}

const timeFormat = "02.01.2006 15:04:05"

func get() (videos []*Video) {
	resp, err := http.Get("https://mediathekdirekt.de/good.json")
	if err != nil {
		panic(err)
	}

	var videosArray [][]string
	json.NewDecoder(resp.Body).Decode(&videosArray)

	for _, data := range videosArray {
		date, err := time.Parse(timeFormat, data[3]+" "+data[4])
		if err != nil {
			fmt.Println(err)
		}

		vhd := ""
		if data[8] != "" {
			split := strings.SplitN(data[8], "|", 2)
			end, _ := strconv.Atoi(split[0])
			vhd = data[6][0:end] + split[1]

		}

		id := b64.URLEncoding.EncodeToString([]byte(hash(data[6])))

		videos = append(videos, &Video{
			ID:          id,
			Title:       data[1],
			Description: data[5],
			Series:      data[2],
			Broadcaster: data[0],
			LibraryURL:  data[7],
			Video:       data[6],
			VideoHD:     vhd,
			Date:        date.String(),
			date:        date,
		})
	}

	return
}

type timeSlice []*Video

// Forward request for length
func (p timeSlice) Len() int {
	return len(p)
}

// Define compare
func (p timeSlice) Less(i, j int) bool {
	return !p[i].date.Before(p[j].date)
}

// Define swap over an array
func (p timeSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func sortVideos(input []*Video) (videos []*Video) {
	toSortVideos := make(timeSlice, 0, len(input))
	for _, d := range input {
		toSortVideos = append(toSortVideos, d)
	}

	sort.Sort(toSortVideos)

	for _, v := range toSortVideos {
		videos = append(videos, v)
	}

	return
}

func hash(text string) string {
	var runes = []rune(text)

	var hash uint64 = 5381
	var index = len(text)

	for index > 0 {
		index--
		hash = (hash * 33) ^ uint64(runes[index])
	}

	return strconv.Itoa(int(hash >> 0))
}
