package gcf

func paginate(videos []*Video, amountPerPage int) (pages [][]*Video) {
	pages = make([][]*Video, (len(videos)/amountPerPage)+1)
	for i, d := range videos {
		if pages[i/amountPerPage] == nil {
			pages[i/amountPerPage] = make([]*Video, amountPerPage)
		}

		pages[i/amountPerPage][i%amountPerPage] = d
	}

	return
}

func PaginateExport(videos []*Video, amountPerPage int) [][]*Video {
	return paginate(videos, amountPerPage)
}
