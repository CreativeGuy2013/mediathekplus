import React from "react";
import { Link } from "react-router-dom";
import InfiniteScroll from 'react-infinite-scroller';

import { Typography, Card, CardPrimaryAction, TextField } from "rmwc";

import { CircularProgress } from '@rmwc/circular-progress';
import '@rmwc/circular-progress/circular-progress.css';

export class HomeDisplay extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            page: 0,
            query: "",
            status: {
                page: null,
                query: null
            },
            data: [],
            error: null
        }
    }

    componentDidMount() {
        this.fetch()
    }

    search(e) {
        this.setState({ page: 0, error: null, query: e.target.value, data: [] }, () => this.fetch())
    }

    fetch() {
        if ((this.state.page !== this.state.status.page || this.state.query !== this.state.status.query) && !this.state.error) {
            if (this.state.query === "") {
                fetch(`/api/data?page=${this.state.page}`)
                    .then((response) => response.json())
                    .then((data) => {
                        var d = this.state.data
                        d.push(...data)
                        this.setState({ status: { page: this.state.page, query: this.state.query }, data: d })
                    })
                    .catch((error) => {
                        console.log(error)
                        this.setState({ error: error })
                    })
            } else {
                fetch(`/api/search?page=${this.state.page}&query=${this.state.query}`)
                    .then((response) => response.json())
                    .then((data) => {
                        var d = this.state.data
                        d.push(...data)
                        this.setState({ status: { page: this.state.page, query: this.state.query }, data: d })
                    })
                    .catch((error) => {
                        console.log(error)
                        this.setState({ error: error })
                    })
    
            }
        }
    }

    render() {
        console.log(this.state)
        return (
            <div className="aui container">
                <TextField label="Search..." style={{ width: "100%" }} value={this.state.query} onChange={(e) => this.search(e)} />
                <InfiniteScroll
                    pageStart={0}
                    loadMore={(page) => {
                        this.setState({ page: this.state.page + 1 }, () => this.fetch())
                    }}
                    hasMore={(this.state.page === this.state.status.page && this.state.query === this.state.status.query) && !this.state.error}
                    loader={<CircularProgress size="small"></CircularProgress>}
                >
                    {this.state.data ? this.state.data.map(d => (
                        <Card key={d.id} style={{ margin: '1rem 0 1rem 0' }}>
                            <CardPrimaryAction tag={Link} to={`/video/${d.id}`}>
                                <div style={{ padding: '0 1rem 1rem 1rem' }}>
                                    <Typography use="headline6" tag="h2">
                                        {d.title}
                                    </Typography>
                                    <Typography
                                        use="subtitle2"
                                        tag="h3"
                                        theme="text-secondary-on-background"
                                        style={{ marginTop: '-1rem' }}
                                    >
                                        {d.series} - {d.broadcaster}
                                    </Typography>
                                    <Typography use="body1" tag="div" theme="text-secondary-on-background">
                                        {d.description}{(d.description.substring(d.description.length - 1) === ".") ? "" : "..."}
                                    </Typography>
                                </div>
                            </CardPrimaryAction>
                        </Card>
                    )) : <></>}
                </InfiniteScroll>
            </div>
        )
    }
}