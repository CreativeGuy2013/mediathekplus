import React from "react";
import { Link } from "react-router-dom";

import {
    TopAppBar,
    TopAppBarRow,
    TopAppBarSection,
    TopAppBarActionItem,
    TopAppBarTitle,
    TopAppBarFixedAdjust
} from '@rmwc/top-app-bar';

export class Navbar extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            location: ""
        }
    }


    render() {
        return (
            <>
                <TopAppBar>
                    <TopAppBarRow>
                        <TopAppBarSection alignStart>
                            <TopAppBarTitle>MediathekPlus</TopAppBarTitle>
                        </TopAppBarSection>
                        <TopAppBarSection alignEnd>
                            <Link to="/" style={{textDecoration: "none"}}>
                                <TopAppBarActionItem aria-label="Home" alt="Home">
                                    home
                                </TopAppBarActionItem>
                            </Link>
                        </TopAppBarSection>
                    </TopAppBarRow>
                </TopAppBar>
                <TopAppBarFixedAdjust />
            </>
        )
    }
}